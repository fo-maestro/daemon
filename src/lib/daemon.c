#include <daemon.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <lang.h>

void (*sighup)(int);
void (*sigterm)(int);

void daemon_sighup_handler(void (*handler)(int signal))
{
    sighup = handler;
}
void daemon_sigterm_handler(void (*handler)(int signal))
{
    sigterm = handler;
}

void log_message(char *message)
{
    FILE *log;
    log = fopen(LOG_FILE, "a");

    if (!log)
    {
        perror("open log:");

        exit(-1);
    }

    fprintf(log, "%s\n", message);
    fclose(log);
}

void deamonize(void)
{
    int i, fd, pid, lfp;
    char str[10];

    pid = fork();

    if (pid < 0)
    {
        exit(1);
    }

    if (pid > 0)
    {
        exit(0);
    }

    setsid();

    for (i = getdtablesize(); i >= 0; --i)
    {
        close(i);
    }

    fd = open("/dev/null", O_RDWR);
    dup(fd);
    dup(fd);
    dup(fd);

    umask(027);
    chdir(RUNNING_DIR);

    lfp = open(LOCK_FILE, O_RDWR | O_CREAT, 0640);

    if (lfp < 0)
    {
        exit(1);
    }

    if (lockf(lfp, F_TLOCK, 0) < 0)
    {
        exit(0);
    }

    sprintf(str, "%d\n", getpid());
    write(lfp, str, strlen(str));

    signal(SIGCHLD, SIG_IGN);
    signal(SIGTSTP, SIG_IGN);
    signal(SIGTTOU, SIG_IGN);
    signal(SIGTTIN, SIG_IGN);

    if (sighup == NULL)
    {
        printf("Sighup handler not set\n");

        exit(-1);
    }

    signal(SIGHUP, sighup);

    if (sigterm == NULL)
    {
        printf("Sigterm handler not set\n");

        exit(-1);
    }

    signal(SIGTERM, sigterm);
}