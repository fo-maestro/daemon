/* Created by Felipe Oyarzun. */

#include <sockdef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

bool is_valid_ip_address(char *ip)
{
    struct sockaddr_in sa;
    int result = inet_pton(AF_INET, ip, &(sa.sin_addr));

    return result != 0;
}

int get_port(connection_t connection)
{
    struct sockaddr_in sin;
    socklen_t len = sizeof(sin);
    getsockname(connection.connection, (struct sockaddr*)&sin, &len);

    return ntohs(sin.sin_port);
}

char *get_ip_address(char *hostname)
{
    struct hostent *he;
    struct in_addr addr;

    if ((he = gethostbyname(hostname)) == NULL)
    {
        return "";
    }

    memcpy(&addr, he->h_addr_list[0], he->h_length);

    return inet_ntoa(addr);
}

void force_write(int fd, void *buf, size_t count)
{
    if (write(fd, buf, count) < 0)
    {
         perror("Error writing to socket");

        exit(EXIT_FAILURE);
    }
}

void force_read(int fd, void *buf, size_t count)
{
    if (read(fd, buf, count) < 0)
    {
        perror("Error reading to socket");

        exit(EXIT_FAILURE);
    }
}