/* Created by Felipe Oyarzun. */

#include <server.h>
#include <strings.h>

connection_t open_server(char* address, uint16_t port, int backlog)
{
    connection_t con;
    struct sockaddr_in addr;

    /* Creacion del socket. */
    if ((con.connection = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        con.status = SOCK_FAILED;

        return con;
    }

    /* Inicializacion de la estructura con los datos de coneccion. */
    bzero((char*)&addr, sizeof(addr));
    addr.sin_addr.s_addr = inet_addr(address);
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    con.address = addr;

    /* Asocia la direccion y el puerto de la estructura al socket creado. */
    if (bind(con.connection, (struct sockaddr*)&con.address, sizeof(con.address)) < 0)
    {
        con.status = SOCK_BIND_FAILED;

        return con;
    }

    con.status = SOCK_OK;

    /* Comienza a escuchar por conecciones entrantes. */
    listen(con.connection, backlog);

    return con;
}

connection_t accept_client(connection_t server)
{
    connection_t client;

    /* Inicializacion de la estructura e coneccion. */
    bzero((char*)&client.address, sizeof(client.address));
    socklen_t length = sizeof(client.address);

    /* Acepta el cliente entrante. */
    client.connection = accept(server.connection, (struct sockaddr*)&client.address, &length);

    if (client.connection == -1)
    {
        client.status = SOCK_FAILED;

        return client;
    }

    client.status = SOCK_OK;

    return client;
}
