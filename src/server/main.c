#include <stdio.h>
#include <server.h>
#include <signal.h>
#include <lang.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <daemon.h>
#include <pthread.h>

#define BUFFER_SIZE 128
#define NUM_REGIONES 16

typedef struct reg
{
    int numhogares;
    int numresidentes;
    int numhombres;
    int nummujeres;
}Region;

typedef struct data_base
{
    char *rut;
    char *nombre;
    char *tipo;
}DataBase;

DataBase *censo;
Region regiones[NUM_REGIONES];
int censo_size = 0;
pthread_mutex_t lock;

void *client_handler(void *arg)
{
    connection_t *client = arg;
    char buffer[BUFFER_SIZE];

    force_read(client->connection, buffer, BUFFER_SIZE);

    DataBase censista = (DataBase) {"", "", ""};

    int i;
    for (i = 0; i < censo_size; i++)
    {
        if (strcmp(censo[i].rut, buffer) == 0)
        {
            censista = censo[i];
            break;
        }
    }

    if (strlen(censista.rut) == 0)
    {
        force_write(client->connection, "El censista ingresado no esta registrado en la base de datos", BUFFER_SIZE);

        log_message("Client Close");
        log_message("Waiting for Connection...");
        pthread_exit(NULL);
    }
    else
    {
        force_write(client->connection, "OK", BUFFER_SIZE);
    }

    bzero(buffer, BUFFER_SIZE);

    force_read(client->connection, buffer, BUFFER_SIZE);

    if (strcmp(buffer, censista.tipo) != 0)
    {
        force_write(client->connection, "El tipo ingresado no corresponde al censista inscrito en la base de datos", BUFFER_SIZE);

        log_message("Client Close");
        log_message("Waiting for Connection...");
        pthread_exit(NULL);
    }
    else
    {
        force_write(client->connection, "OK", BUFFER_SIZE);
    }

    int value;
    force_write(client->connection, "Ingrese numero de Region:", BUFFER_SIZE);
    force_read(client->connection, &value, BUFFER_SIZE);

    int region = value;

    Region taken = (Region) {0, 0, 0, 0};

    bzero(buffer, BUFFER_SIZE);
    force_write(client->connection, "Ingrese el numero de Hogares a censar:", BUFFER_SIZE);
    force_read(client->connection, &value, BUFFER_SIZE);

    taken.numhogares = value;

    for (i = 0; i < taken.numhogares; i++)
    {
        force_write(client->connection, "Ingrese numero de habitantes:", BUFFER_SIZE);
        force_read(client->connection, &value, sizeof(int));
        taken.numresidentes += value;

        force_write(client->connection, "Ingrese numero de hombres:", BUFFER_SIZE);
        force_read(client->connection, &value, sizeof(int));
        taken.numhombres += value;

        force_write(client->connection, "Ingrese numero de mujeres:", BUFFER_SIZE);
        force_read(client->connection, &value, sizeof(int));
        taken.nummujeres += value;
    }

    pthread_mutex_lock(&lock);
    regiones[region].numhogares += taken.numhogares;
    regiones[region].numresidentes += taken.numresidentes;
    regiones[region].numhombres += taken.numhombres;
    regiones[region].nummujeres += taken.nummujeres;
    pthread_mutex_unlock(&lock);

    force_write(client->connection, "OK", BUFFER_SIZE);

    close(client->connection);
    
    log_message("Client Close");
    log_message("Waiting for Connection...");
    pthread_exit(NULL);
}

void init_regiones(void)
{
    int i;
    for (i = 0; i < NUM_REGIONES; i++)
    {
        regiones[i] = (Region) {0, 0, 0, 0};
    }
}

void load_db(void)
{
    FILE *db = fopen("resources/censistas.db", "r");

    if (db == NULL)
    {
        log_message("No se puede encontrar el archivo \"censistas.db\" en la carpeta \"resources\"\n");

        exit(EXIT_FAILURE);
    }

    int i;
    for (i = 1; !feof(db); i++)
    {
        censo = realloc(censo, sizeof(DataBase) * i);

        censo[i - 1].rut = malloc(BUFFER_SIZE);
        censo[i - 1].nombre = malloc(BUFFER_SIZE);
        censo[i - 1].tipo = malloc(BUFFER_SIZE);

        fscanf(db, "%s%s%s", censo[i - 1].rut, censo[i - 1].nombre, censo[i - 1].tipo);

        censo[i - 1].rut = realloc(censo[i - 1].rut, strlen(censo[i - 1].rut) + 1);
        censo[i - 1].nombre = realloc(censo[i - 1].nombre, strlen(censo[i - 1].nombre) + 1);
        censo[i - 1].tipo = realloc(censo[i - 1].tipo, strlen(censo[i - 1].tipo) + 1);
    }

    censo_size = i - 1;

    fclose(db);
}

int main(void)
{
    pthread_mutex_init(&lock, NULL);
    connection_t server;

    daemon_sighup_handler(lambda(void, (int signal), {
        log_message("Server Restart");
    }));

    daemon_sigterm_handler(lambda(void, (int signal), {
        close(server.connection);

        pthread_t thread;
        pthread_create(&thread, NULL, lambda(void*, (), {
            pthread_mutex_lock(&lock);
            log_message("Censo Final Report:");
            int i;
            for (i = 0; i < NUM_REGIONES; i++)
            {
                char buffer[BUFFER_SIZE];
                if (i == 13) continue;

                sprintf(buffer, "Region: %d Hogares: %d Residentes: %d Hombres: %d Mujeres: %d",
                    i, regiones[i].numhogares, regiones[i].numresidentes,
                    regiones[i].numhombres, regiones[i].nummujeres);

                log_message(buffer);
            }

            pthread_mutex_unlock(&lock);
            pthread_mutex_destroy(&lock);

            pthread_exit(NULL);
        }), NULL);

        pthread_join(thread, NULL);
        
        log_message("Server Off");
        exit(EXIT_SUCCESS);
    }));

    load_db();
    deamonize();

    log_message("Server On");
    log_message("Initializing Data...");
    init_regiones();
    log_message("Data initialized");
    log_message("Opening Coneccion...");
    server = open_server("127.0.0.1", 0, 5);

    if (server.status == SOCK_FAILED)
    {
        log_message("Error: Failed to create Socket\n");

        return EXIT_FAILURE;
    }
    else if (server.status == SOCK_BIND_FAILED)
    {
        log_message("Error: Failed to Bind Socket\n");

        return EXIT_FAILURE;
    }
    
    log_message("Connection Open");

    log_message("Server: localhost\n");
    char buff[16];
    sprintf(buff, "Port: %d\n", get_port(server));
    log_message(buff);

    for (;;)
    {
        log_message("Waiting for Connection...");
        connection_t *client = malloc(sizeof(connection_t));
        *client = accept_client(server);
        log_message("Connection Stablished");

        pthread_t thread;
        pthread_create(&thread, NULL, client_handler, client);
    }

    return 0;
}