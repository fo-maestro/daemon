/* Created by Felipe Oyarzun. */

#include <client.h>
#include <strings.h>

connection_t open_client(char* address, uint16_t port)
{
    connection_t con;

    /* Creacion del socket. */
    if ((con.connection = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        /* Creacion del socket fallida. */
        con.status = SOCK_FAILED;

        return con;
    }

    /* Inicializacion de la estructura con los datos de coneccion. */
    bzero((char*)&con.address, sizeof(con.address));
    con.address.sin_addr.s_addr = inet_addr(address);
    con.address.sin_family = AF_INET;
    con.address.sin_port = htons(port);

    /* Creacion del socket sin fallos. */
    con.status = SOCK_OK;

    return con;
}

int connect_server(connection_t server)
{
    return connect(server.connection, (struct sockaddr*)&server.address, sizeof(server.address));
}
