#include <stdio.h>
#include <client.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#define BUFFER_SIZE 128

void validate_params(int argc, char **argv)
{
    if (argc != 5)
    {
        printf("Ussage: client [host name] [port] [rut] [c|s]\n");

        exit(EXIT_FAILURE);
    }

    if (strlen(get_ip_address(argv[1])) == 0)
    {
        printf("No such host\n");

        exit(EXIT_FAILURE);
    }

    if (atoi(argv[2]) <= 0)
    {
        printf("Invalid Port Number\n");

        exit(EXIT_FAILURE);
    }
}

int main(int argc, char **argv)
{
    validate_params(argc, argv);

    char *ip_address = get_ip_address(argv[1]);

    connection_t server = open_client(ip_address, atoi(argv[2]));

    char buffer[BUFFER_SIZE];

    if (connect_server(server) == -1)
    {
        perror("Failed to Connect");

        return EXIT_FAILURE;
    }

    force_write(server.connection, argv[3], BUFFER_SIZE);

    force_read(server.connection, buffer, BUFFER_SIZE);

    if (strcmp(buffer, "OK") != 0)
    {
        printf("%s\n", buffer);

        close(server.connection);
        exit(EXIT_FAILURE);
    }

    force_write(server.connection, argv[4], BUFFER_SIZE);

    bzero(buffer, BUFFER_SIZE);

    force_read(server.connection, buffer, BUFFER_SIZE);

    if (strcmp(buffer, "OK") != 0)
    {
        printf("%s\n", buffer);

        close(server.connection);
        exit(EXIT_FAILURE);
    }

    int value;

    for (;;)
    {
        bzero(buffer, BUFFER_SIZE);
        force_read(server.connection, buffer, BUFFER_SIZE);

        if (strcmp(buffer, "OK") == 0)
        {
            close(server.connection);

            exit(EXIT_SUCCESS);
        }

        printf("%s ", buffer);

        scanf("%d", &value);
        force_write(server.connection, &value, sizeof(int));
    }

    return 0;
}