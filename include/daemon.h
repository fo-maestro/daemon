#ifndef _DAEMON_H
#define _DAEMON_H

#define RUNNING_DIR "/tmp"
#define LOCK_FILE "daemon.lock"
#define LOG_FILE "daemon.log"

void daemon_sighup_handler(void (*handler)(int));
void daemon_sigterm_handler(void (*handler)(int));
void log_message(char *message);
void deamonize(void);

#endif /* _DAEMON_H */