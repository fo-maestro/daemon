/* Created by Felipe Oyarzun. */

#ifndef _SERVER_H
#define _SERVER_H

#include <sockdef.h>

/* Abre una conexion de tipo server a la espera de conecciones entrantes. */
connection_t open_server(char* address, uint16_t port, int backlog);

/* Acepta una coneccion entrante de tipo cliente. */
connection_t accept_client(connection_t server);

#endif /* _SERVER_H */
