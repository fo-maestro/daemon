/* Created by Felipe Oyarzun. */

#ifndef _CLIENT_H
#define _CLIENT_H

#include <sockdef.h>

/* Abre una conexion con el servidor y devuelve la esrtuctura con datos de este. */
connection_t open_client(char* address, uint16_t port);

/* Connecta al servidor especificado y devuelve el estado de la solicitud. */
int connect_server(connection_t server);

#endif /* _CLIENT_H */
