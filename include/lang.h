/* Created by Felipe Oyarzun. */

#ifndef _LANG_H
#define _LANG_H

/* Define una funcion lambda para la creacion de funciones anonimas. */
#define lambda(l_ret_type, l_params, l_body) \
    ({ \
        l_ret_type l_annonymous_func l_params \
        l_body \
        l_annonymous_func; \
    })

#endif /* _LANG_H */