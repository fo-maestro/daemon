/* Created by Felipe Oyarzun. */

#ifndef _SOCKDEF_H
#define _SOCKDEF_H

#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <stdbool.h>
#include <unistd.h>

/* Enum que describe los diferentes estados en el momento de la creacion de un socket. */
typedef enum _status
{
    SOCK_OK, SOCK_FAILED, SOCK_BIND_FAILED
}status_t;

/* Estructura que almacena la coneccion de un socket, su direccion y ip y su puerto asociado,
   ademas del estado al momento de su creacion. */
typedef struct _connection
{
    int connection;
    struct sockaddr_in address;
    status_t status;
}connection_t;

/* Devuelve true si la direccion ip ingresada corresponde a una ip valida. */
bool is_valid_ip_address(char *ip);

int get_port(connection_t connection);

char *get_ip_address(char *hostname);

void force_write(int fd, void *buf, size_t count);

void force_read(int fd, void *buf, size_t count);

#endif /* _SOCKDEF_H */
